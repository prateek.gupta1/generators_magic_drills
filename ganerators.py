
def gen_fun(start, stop, step):
    while(start != stop):
        yield(start)
        start = start + step


# gen_fun(5, 100, 5)
# gen_fun(30, 3, -3)


def gen_zip(*iterables):
#    print(iterables)
    item = object()
    iter_item = [iter(i) for i in iterables]
#    print(item)
    while iter_item:
        res = []
        for i in iter_item:
            elem = next(i, item)
            if elem is item:
                return
            res.append(elem)
        yield(tuple(res))


gen_zip('ABC', 'MN')
