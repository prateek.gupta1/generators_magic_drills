import random

print((5).__add__(78))


list = [1, 5, 8, 'prats', 'likhit']
print(list.__len__())


str = "Magic Methods"
print(str.__len__())

list1 = [12, 45.5, 778]
list2 = ['vishal', 'sagar']
print(list1.__add__(list2))


class BankAccount:
    def __init__(self, id, balance):
        self.id = id
        self.balance = balance

    def __lt__(self, other):
        if self < other:
            return True
        else:
            return False

    def __eq__(self, other):
        if self.id == other.id and self.balance == other.balance:
            return True
        else:
            return False
    
    def __repr__(self):
        return "BankAccount(id={}, balance={})".format(self.id, self.balance)


if __name__ == '__main__':
    accounts = [BankAccount(id=i, balance=random.randint(1, 10000)) for i in range(10)]
    for i in range(len(accounts)):
        for j in range(i, len(accounts)):
            if accounts[i].balance.__lt__(accounts[j].balance):
                accounts[j], accounts[i] = accounts[i], accounts[j]
    print(accounts)
    print(accounts[5].__eq__(accounts[9]))


class Bank(BankAccount):

    def __init__(self):
        self.accounts = []

    def add_account(self, account):
        self.accounts.append(account)

    def __iter__(self):
        self.id = 0
        return self

    def __next__(self):
        if self.id == (self.accounts).__len__():
            raise StopIteration
        self.id += 1
        return bank.accounts[i]


if __name__ == '__main__':
    bank = Bank()
    itr = iter(bank)
    for i in range(10):
        bank.add_account(BankAccount(id=i, balance=i * 100))
    for account in iter(bank):
        print(account)
